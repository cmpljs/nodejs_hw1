const http = require('http');
const fs = require('fs');
const url = require('url');

module.exports = () => {

const server = http.createServer();

server.on('error', err => {
    res.writeHead(500, {'Content-Type': 'text/html'});
    res.write('Internal server error');
    res.end();
});

server.on('request', (req, res) => {

    let { pathname, query } = url.parse(req.url, true);

    if (!pathname.includes('/file')) {
        pathname = '/file' + pathname;
    }

    let { logs } = fs.readFileSync('./file/logs.json', 'utf-8') 
                   ? JSON.parse(fs.readFileSync('./file/logs.json', 'utf-8'))
                   : {logs: []};

    const sendResponse = (code, message, contentType = 'text/html') => {
        res.writeHead(code, {'Content-Type': contentType});
        res.write(message);
        res.end();
    }

    const writeLogs = (message) => {
        logs.push({message, time: Date.now()});
        fs.writeFile('./file/logs.json', JSON.stringify({logs}, null, '  '), 'utf-8', () => {});
    }

    const validateQueryParams = (filename, content) => {
        const extension = '^.*\.(txt|TXT|doc|DOC|rtf|RTF|pdf|PDF)$';
        const forbidden = '[^a-zA-Z0-9]';
        const name = filename?.substr(0, filename?.indexOf('.'));

        if (!content || !filename) {
            sendResponse(400, 'Required query params are not specified');

            return false;
        }  

        if (name.match(forbidden)) {
            sendResponse(400, 'Filename param contains forbidden characters');

            return false;
        }

        if (!filename.match(extension)) {
            sendResponse(400, 'Invalid filename param extension');

            return false;
        }

        return true;
    }

    const porcessPostReq = () => {

        fs.writeFile('./file/' + query.filename, query.content, 'utf8', () => {});
        writeLogs(`New file with name ${query.filename} has been saved`);
        sendResponse(200, `New file with name ${query.filename} has been saved`);
    }

    const processGetReq = () => {

        if (pathname === '/file/logs') {

            logs = logs.filter(log => log.time >= (+query.from || 0)  
                                                && log.time <= (+query.to || Infinity));
            sendResponse(200, JSON.stringify({logs: logs}, null, '  '), 'application/json')

            return;   
        } 

        fs.readFile(`./${pathname}`, (err, content) => {

            if (err) {
                sendResponse(400, 'No such file or directory: ' + pathname);
    
                return;
            }

            writeLogs(`File ${pathname.substr(1)} has been read`);
            sendResponse(200, content);
        
            return;
        });
    }

    const processDeleteReq = () => {

        fs.unlink(`./${pathname}`, err => {

            if (err) {
                sendResponse(400, 'No such file or directory: ' + pathname);

                return;
            }
            
            writeLogs(`File ${pathname.substr(1)} has been deleted`);
            sendResponse(200, `File with name ${pathname.substr(1)} has been deleted`);

            return;
        });
    }


    
    switch (req.method) {

    case 'POST': {

        if (!validateQueryParams(query.filename, query.content)) { 
            break;
        };

        porcessPostReq();
        
        break;
    }

    case 'GET': {

        processGetReq();
        
        break;
    }

    case 'DELETE': {
       
        processDeleteReq();

        break;
    }

    default: sendResponse(500 , 'Unsupported request method');
}

});

server.listen(process.env.PORT || 8080);

};